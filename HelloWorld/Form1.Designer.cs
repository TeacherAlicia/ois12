﻿namespace HelloWorld
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbHuman1 = new System.Windows.Forms.Label();
            this.lbHuman2 = new System.Windows.Forms.Label();
            this.lbHuman3 = new System.Windows.Forms.Label();
            this.lbTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbHuman1
            // 
            this.lbHuman1.AutoSize = true;
            this.lbHuman1.Location = new System.Drawing.Point(91, 174);
            this.lbHuman1.Name = "lbHuman1";
            this.lbHuman1.Size = new System.Drawing.Size(51, 20);
            this.lbHuman1.TabIndex = 0;
            this.lbHuman1.Text = "label1";
            // 
            // lbHuman2
            // 
            this.lbHuman2.AutoSize = true;
            this.lbHuman2.Location = new System.Drawing.Point(392, 425);
            this.lbHuman2.Name = "lbHuman2";
            this.lbHuman2.Size = new System.Drawing.Size(51, 20);
            this.lbHuman2.TabIndex = 1;
            this.lbHuman2.Text = "label1";
            // 
            // lbHuman3
            // 
            this.lbHuman3.AutoSize = true;
            this.lbHuman3.Location = new System.Drawing.Point(639, 174);
            this.lbHuman3.Name = "lbHuman3";
            this.lbHuman3.Size = new System.Drawing.Size(51, 20);
            this.lbHuman3.TabIndex = 2;
            this.lbHuman3.Text = "label1";
            // 
            // lbTotal
            // 
            this.lbTotal.AutoSize = true;
            this.lbTotal.Location = new System.Drawing.Point(785, 665);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(51, 20);
            this.lbTotal.TabIndex = 3;
            this.lbTotal.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 736);
            this.Controls.Add(this.lbTotal);
            this.Controls.Add(this.lbHuman3);
            this.Controls.Add(this.lbHuman2);
            this.Controls.Add(this.lbHuman1);
            this.Name = "Form1";
            this.Text = "s";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbHuman1;
        private System.Windows.Forms.Label lbHuman2;
        private System.Windows.Forms.Label lbHuman3;
        private System.Windows.Forms.Label lbTotal;
    }
}

