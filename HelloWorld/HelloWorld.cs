﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelloWorld
{
    public partial class Form1 : Form
    {
        //Objecten
        Human human1;
        Human human2;
        Human human3;

        int doubleAge = 20;
        int age;

        public Form1()
        {
            InitializeComponent();

            human1 = new Human();
            human2 = new Human("Kabouters lopen door het bos");
            human3 = new Human("Trollen wonen in een donker bos");

            lbHuman1.Text = human1.Sentence;
            lbHuman2.Text = human2.Sentence;
            lbHuman3.Text = human3.Sentence;

            lbTotal.Text = (human1.CountCharacters() + human2.CountCharacters() + human3.CountCharacters()).ToString();
        }

    }
}
