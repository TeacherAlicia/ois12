﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorld
{
    class Human
    {
        //variables/fields
        private string sentence;

        //properties
        public string Sentence
        {
            get
            {
                return this.sentence;
            }
            private set
            {
                this.sentence = value;
            }
        }

        //constructor
        public Human(string p_sentence = "Hello World")
        {
            //Console.WriteLine("blabla");
            this.Sentence = p_sentence;
        }
        
        //methoden
        public int CountCharacters()
        {
            return this.Sentence.Length;
        }
    }
}
