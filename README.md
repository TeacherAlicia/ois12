# OIS12

## Hello World App - (OOP) Classes, Objects, Methods, Constructors, Properties, Fields, Private/Public
**Niveau beginner**

In deze applicatie kan je de basis van OOP zien. Waar bestaat dat uit?
* Verschil tussen een klasse en object
* Opbouw van een klasse met daarin
    * Fields / variabelens
    * Properties
    * Constructor
    * Methods
* Het aanmaken van een object

In deze app heb je de Form (is ook een klasse) HelloWorld. Deze form moet de echte wereld voorstellen. Hierin zitten 4 labels (lbHuman1, lbHuman2, lbHuman3 en lbTotal). De eerste 3 labels
worden gevuld met sentences (zinnen) uit de Human objecten.

### Extra oefening
* Voeg een extra field "Age" (met property!) aan de Human klasse toe. Laat de "Age" op een of andere manier op de Hello World scherm zien.
* Voeg een extra method aan de Human klasse toe. Met deze method kan je de helft van de leeftijd berekenen.
* Voeg een extra klasse "Dog" toe. Deze klasse moeten de twee fields "dogbreed" (ras) en "name"  hebben.
* Bedenk zelf nog iets cools!


## Hello Lists App - OOP, ToString, Casting, List
**Niveau beginner**

In deze app kan je zien hoe ToString, Casting en lists werken.

Deze app is een uitbreiding van de Hello World app. Deze app moet de wereld voorstellen. In de wereld heb je allerlei objecten met verschillende objecten.
Je hebt verschillende (objecten) mensen (een klasse) met zijn of haar eigen eigenschappen (fields). Maar denk ook aan verschillende planten, dieren en gebouwen.

In deze app zitten twee listboxen. In de ene listbox zitten plant-objecten en de andere human-objecten. Bij de plant-objecten is niet de ToString methode gebruikt, maar bij de human-objecten wel.
Daarnaast kan je zelf human objecten toevoegen. De delete en edit knoppen doen het (nog) niet.

### Extra oefening
* Nu worden de planten niet goed weergegeven in de listbox. Zorg ervoor dat in de listbox de naam en kleur te zien is (dmv ToString).
* Zorg ervoor dat de verwijder en edit knop het doet
* Voeg extra velden en knoppen voor de planten toe. En zorg ervoor dat je planten kan toevoegen, verwijderen en kan aanpassen.


## Muziekcollectie App - OOP, ToString, Casting, List
**Niveau expert**

In deze app kan je zien hoe ToString, Casting en lists werken, maar dan niveau "next level".

Met de muziekcollectie app kan je een overzicht zien van songs met artiest. Je kunt zelf artiesten en songs toevoegen, aanpassen en verwijderen om zo zelf een muziekcollectie te maken.
De songs staan in een listbox en zijn er dmv de ToString-method als object ingezet. Door casting zijn de objecten weer uit de listbox te halen.

### Extra oefening
* Laat alleen de titel van de song zien in de listbox overzicht
* Voeg een methode toe die van de lengte van de song minuten maakt (het staat er nu in seconde bij)
* Voeg een extra "genre" klasse toe en zorg ervoor dat je deze kan toevoegen aan de song


## Hello File App - File handling en Exceptions
**Niveau beginner**

Een simpele app waarin het tekstbestand "Helloworld" wordt uitgeladen. Er zit een try en catch check op (exceptions).


## Hello Globe App - GDI Graphics
**Niveau beginner**

In deze app kan je zien hoe je via GDI Graphics rectangles en ellipses kan toevoegen.

### Extra oefening
* Voeg zelf een rectangles en ellipes toe
* Pas de methods zo aan dat je ook de kleur kan bepalen
* Voeg een method toe waardoor je een line kan toevoegen


## Hello Method Construct - Method overloading & Method overloading bij Constructors
**Niveau beginner**

In deze app kan je zien hoe method overloading (bij constructors) werkt.
In de form klass (HelloMethodConstruct.cs) staat drie keer de method "Add". Elke "Add" method heeft andere parameters. Hierdoor ontstaat er method overloading.
Naast deze form class is er ook een Human class. In deze class zijn er twee constructors. Eentje met 1 parameter (sentence) en eentje met twee (sentence en age).

### Extra oefening
* Voeg zelf een Add method toe
* Voeg zelf een constructor aan de Human class toe

