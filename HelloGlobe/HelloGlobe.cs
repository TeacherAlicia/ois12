﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelloGlobe
{
    public partial class HelloGlobe : Form
    {
        public HelloGlobe()
        {
            InitializeComponent();
        }

        private void CreateDrawRectangle(float width = 10, float height = 10, int positionX = 10, int positionY = 10)
        {
            Graphics rectangle = CreateGraphics();
            rectangle.DrawRectangle(Pens.Black, positionX, positionY, width, height);
        }

        private void CreateFillRectangle(float width = 10, float height = 10, int positionX = 10, int positionY = 10)
        {
            Graphics rectangle = CreateGraphics();
            rectangle.FillRectangle(Brushes.Red, positionX, positionY, width, height);
        }

        private void CreateFillEllipse(float width = 10, float height = 10, int positionX = 10, int positionY = 10)
        {
            Graphics ellipse = CreateGraphics();
            ellipse.FillEllipse(Brushes.Blue, positionX, positionY, width, height);
        }

        private void CreatePainting(object sender, PaintEventArgs e)
        {
            this.CreateFillEllipse(this.Width - 50, this.Height - 50, 15, 5);
            this.CreateDrawRectangle(300, 200, 200, 200);
            this.CreateFillRectangle(100, 50, 100, 200);
        }
    }
}
