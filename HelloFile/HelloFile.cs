﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;//toevoegen System.IO

namespace HelloFile
{
    public partial class HelloFile : Form
    {
        StreamReader HelloWorld;

        public HelloFile()
        {
            InitializeComponent();

            try //als er iets fout gaat in dit blok, slaat het programma het blok over.
            {
                HelloWorld = new StreamReader(@"<path naar bestand>");

                string line;
                while ((line = HelloWorld.ReadLine()) != null)
                {
                    tbHelloWorld.Text += line + "\n";
                }

                HelloWorld.Close();
            }
            catch (Exception exc)//is er iets fout gegaan in de try block, zie je de foutmelding in de console (Console.Writeline(esc)).
            {
                Console.WriteLine(exc);
            }
            finally //dit voert de try en catch altijd uit
            {
                Console.WriteLine("hello world");
            }
        }

    }
}
