﻿namespace HelloFile
{
    partial class HelloFile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbHelloWorld = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // tbHelloWorld
            // 
            this.tbHelloWorld.Location = new System.Drawing.Point(12, 12);
            this.tbHelloWorld.Name = "tbHelloWorld";
            this.tbHelloWorld.Size = new System.Drawing.Size(582, 508);
            this.tbHelloWorld.TabIndex = 0;
            this.tbHelloWorld.Text = "";
            // 
            // HelloFile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(606, 540);
            this.Controls.Add(this.tbHelloWorld);
            this.Name = "HelloFile";
            this.Text = "HelloFile";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox tbHelloWorld;
    }
}

