﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muziekcollectie
{
    class Artist
    {
        //Fields
        private string name;
        private List<Song> songList = new List<Song>();

        //Properties
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public List<Song> SongList
        {
            get
            {
                return this.songList;
            }
            set
            {
                this.songList = value;
            }
        }

        //Constructor
        public Artist()
        { 
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
