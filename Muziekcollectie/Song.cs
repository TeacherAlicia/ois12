﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Muziekcollectie
{
    class Song
    {
        //Fields
        private string name;
        private int length;
        private Artist artist;

        //Properties
        public string Name 
        {
            get 
            {
                return this.name;
            }

            set
            {
                this.name = value;
            }
        }

        public int Length
        {
            get
            {
                return this.length;
            }
            set
            {
                this.length = value;
            }
        }

        public Artist Artist
        {
            get
            {
                return this.artist;
            }
            set
            {
                this.artist = value;
            }
        }


        //Constructor
        public Song()
        {
        }

        //Methods


        //ToString: met een ToString methode kan je je object een waarde meegeven die uit gelezen kan worden als string
        public override string ToString()
        {
            return this.Artist.Name + " // " + this.Name + " - " + this.Length;
        }
    }
}
