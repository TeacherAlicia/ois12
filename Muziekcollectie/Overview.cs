﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Muziekcollectie
{
    public partial class Overview : Form
    {
        List<Artist> artistList = new List<Artist>();

        public Overview()
        {
            InitializeComponent();

            this.SetSongList();
            this.SetOverview();
        }

        private void SetOverview()
        {
            cbSongArtist.Items.Clear();
            lbList.Items.Clear();
            tbArtistName.Text = "";
            tbSongName.Text = "";
            tbSongLength.Text = "";
            cbSongArtist.SelectedItem = null;


            foreach (Artist artist in this.artistList)
            {
                cbSongArtist.Items.Add(artist);

                if (artist.SongList != null)
                {
                    foreach (Song song in artist.SongList)
                    {
                        lbList.Items.Add(song);
                    }
                }
            }
        }

        private void lbList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Casting: wanneer je een object in een listbox stopt en je het er weer uithaalt moet je duidelijk maken
            //wat voor object het is. Dit doe je met casting.
            Song song = (Song)lbList.SelectedItem;

            tbSongName.Text = song.Name;
            tbSongLength.Text = song.Length.ToString();
            tbArtistName.Text = song.Artist.Name;
            cbSongArtist.SelectedItem = song.Artist;
        }


        private void btSongAdd_Click(object sender, EventArgs e)
        {
            Artist selectedArtist = (Artist)cbSongArtist.SelectedItem;

            selectedArtist.SongList.Add(new Song() { Name = tbSongName.Text, Length = Convert.ToInt32(tbSongLength.Text), Artist = selectedArtist });
            this.SetOverview();
        }

        private void btSongDelete_Click(object sender, EventArgs e)
        {
            Song selectedSong = (Song)lbList.SelectedItem;
            Artist selectedArtist = selectedSong.Artist;

            selectedArtist.SongList.Remove(selectedSong);
            this.SetOverview();
        }

        private void btSongEdit_Click(object sender, EventArgs e)
        {
            Song selectedSong = (Song)lbList.SelectedItem;

            selectedSong.Name = tbSongName.Text;
            selectedSong.Length = Convert.ToInt32(tbSongLength.Text);

            this.SetOverview();
        }

        private void btArtistAdd_Click(object sender, EventArgs e)
        {
            artistList.Add(new Artist() { Name = tbArtistName.Text });
            this.SetOverview();
        }

        private void btArtistDelete_Click(object sender, EventArgs e)
        {
            Song selectedSong = (Song)lbList.SelectedItem;
            Artist selectedArtist = selectedSong.Artist;

            this.artistList.Remove(selectedArtist);
            this.SetOverview();
        }

        private void btArtistEdit_Click(object sender, EventArgs e)
        {
            Song song = (Song)lbList.SelectedItem;

            song.Artist.Name = tbArtistName.Text;
            this.SetOverview();
        }


        private void SetSongList()
        {
            //Handmatig artiesten met songs toevoegen (normaal uit een db)
            Artist Aqua = new Artist() { Name = "Aqua" };
            Aqua.SongList = new List<Song>() { new Song() { Name = "Barbie Girl", Length = 32, Artist = Aqua } };

            Artist SpiceGirls = new Artist() { Name = "Spice Girls" };
            SpiceGirls.SongList = new List<Song>()
            {
                new Song(){ Name = "Stop", Length = 432, Artist = SpiceGirls },
                new Song(){ Name = "Wannabe", Length = 43, Artist = SpiceGirls },
                new Song(){ Name = "2 Become 1", Length = 79, Artist = SpiceGirls },
                new Song(){ Name = "Viva Forever", Length = 64, Artist = SpiceGirls }
            };

            Artist Scatman = new Artist() { Name = "Scatman" };
            Scatman.SongList = new List<Song> { new Song() { Name = "I'm a Scatman", Length = 32, Artist = Scatman } };

            Artist Hanson = new Artist() { Name = "Hanson" };
            Hanson.SongList = new List<Song> { new Song() { Name = "MmmBop", Length = 32, Artist = Hanson } };


            Artist BackstreetBoys = new Artist { Name = "Backstreet Boys" };
            BackstreetBoys.SongList = new List<Song>()
            {
                new Song(){ Name = "Everybody", Length = 432, Artist = BackstreetBoys },
                new Song(){ Name = "I Want It That Way", Length = 43, Artist = BackstreetBoys }
            };

            artistList.Add(Aqua);
            artistList.Add(SpiceGirls);
            artistList.Add(Scatman);
            artistList.Add(Hanson);
            artistList.Add(BackstreetBoys);
        }
    }
}
