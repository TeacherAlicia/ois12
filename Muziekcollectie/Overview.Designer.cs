﻿namespace Muziekcollectie
{
    partial class Overview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbList = new System.Windows.Forms.ListBox();
            this.lbTitle = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btArtistAdd = new System.Windows.Forms.Button();
            this.btArtistDelete = new System.Windows.Forms.Button();
            this.btArtistEdit = new System.Windows.Forms.Button();
            this.tbArtistName = new System.Windows.Forms.TextBox();
            this.btSongAdd = new System.Windows.Forms.Button();
            this.btSongDelete = new System.Windows.Forms.Button();
            this.btSongEdit = new System.Windows.Forms.Button();
            this.tbSongName = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbSongLength = new System.Windows.Forms.TextBox();
            this.cbSongArtist = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbList
            // 
            this.lbList.FormattingEnabled = true;
            this.lbList.ItemHeight = 20;
            this.lbList.Location = new System.Drawing.Point(12, 112);
            this.lbList.Name = "lbList";
            this.lbList.Size = new System.Drawing.Size(462, 284);
            this.lbList.TabIndex = 0;
            this.lbList.SelectedIndexChanged += new System.EventHandler(this.lbList_SelectedIndexChanged);
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.lbTitle.Location = new System.Drawing.Point(2, 9);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(348, 55);
            this.lbTitle.TabIndex = 1;
            this.lbTitle.Text = "Muziekcollectie";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btArtistAdd);
            this.groupBox1.Controls.Add(this.btArtistDelete);
            this.groupBox1.Controls.Add(this.btArtistEdit);
            this.groupBox1.Controls.Add(this.tbArtistName);
            this.groupBox1.Location = new System.Drawing.Point(480, 112);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(434, 110);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Artist";
            // 
            // btArtistAdd
            // 
            this.btArtistAdd.Location = new System.Drawing.Point(191, 69);
            this.btArtistAdd.Name = "btArtistAdd";
            this.btArtistAdd.Size = new System.Drawing.Size(75, 31);
            this.btArtistAdd.TabIndex = 3;
            this.btArtistAdd.Text = "Add";
            this.btArtistAdd.UseVisualStyleBackColor = true;
            this.btArtistAdd.Click += new System.EventHandler(this.btArtistAdd_Click);
            // 
            // btArtistDelete
            // 
            this.btArtistDelete.Location = new System.Drawing.Point(272, 69);
            this.btArtistDelete.Name = "btArtistDelete";
            this.btArtistDelete.Size = new System.Drawing.Size(75, 31);
            this.btArtistDelete.TabIndex = 2;
            this.btArtistDelete.Text = "Delete";
            this.btArtistDelete.UseVisualStyleBackColor = true;
            this.btArtistDelete.Click += new System.EventHandler(this.btArtistDelete_Click);
            // 
            // btArtistEdit
            // 
            this.btArtistEdit.Location = new System.Drawing.Point(353, 69);
            this.btArtistEdit.Name = "btArtistEdit";
            this.btArtistEdit.Size = new System.Drawing.Size(75, 31);
            this.btArtistEdit.TabIndex = 1;
            this.btArtistEdit.Text = "Edit";
            this.btArtistEdit.UseVisualStyleBackColor = true;
            this.btArtistEdit.Click += new System.EventHandler(this.btArtistEdit_Click);
            // 
            // tbArtistName
            // 
            this.tbArtistName.Location = new System.Drawing.Point(6, 37);
            this.tbArtistName.Name = "tbArtistName";
            this.tbArtistName.Size = new System.Drawing.Size(422, 26);
            this.tbArtistName.TabIndex = 0;
            // 
            // btSongAdd
            // 
            this.btSongAdd.Location = new System.Drawing.Point(191, 106);
            this.btSongAdd.Name = "btSongAdd";
            this.btSongAdd.Size = new System.Drawing.Size(75, 31);
            this.btSongAdd.TabIndex = 3;
            this.btSongAdd.Text = "Add";
            this.btSongAdd.UseVisualStyleBackColor = true;
            this.btSongAdd.Click += new System.EventHandler(this.btSongAdd_Click);
            // 
            // btSongDelete
            // 
            this.btSongDelete.Location = new System.Drawing.Point(272, 106);
            this.btSongDelete.Name = "btSongDelete";
            this.btSongDelete.Size = new System.Drawing.Size(75, 31);
            this.btSongDelete.TabIndex = 2;
            this.btSongDelete.Text = "Delete";
            this.btSongDelete.UseVisualStyleBackColor = true;
            this.btSongDelete.Click += new System.EventHandler(this.btSongDelete_Click);
            // 
            // btSongEdit
            // 
            this.btSongEdit.Location = new System.Drawing.Point(353, 106);
            this.btSongEdit.Name = "btSongEdit";
            this.btSongEdit.Size = new System.Drawing.Size(75, 31);
            this.btSongEdit.TabIndex = 1;
            this.btSongEdit.Text = "Edit";
            this.btSongEdit.UseVisualStyleBackColor = true;
            this.btSongEdit.Click += new System.EventHandler(this.btSongEdit_Click);
            // 
            // tbSongName
            // 
            this.tbSongName.Location = new System.Drawing.Point(6, 37);
            this.tbSongName.Name = "tbSongName";
            this.tbSongName.Size = new System.Drawing.Size(422, 26);
            this.tbSongName.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cbSongArtist);
            this.groupBox2.Controls.Add(this.tbSongLength);
            this.groupBox2.Controls.Add(this.btSongAdd);
            this.groupBox2.Controls.Add(this.btSongDelete);
            this.groupBox2.Controls.Add(this.btSongEdit);
            this.groupBox2.Controls.Add(this.tbSongName);
            this.groupBox2.Location = new System.Drawing.Point(480, 244);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(434, 146);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Song";
            // 
            // tbSongLength
            // 
            this.tbSongLength.Location = new System.Drawing.Point(6, 72);
            this.tbSongLength.Name = "tbSongLength";
            this.tbSongLength.Size = new System.Drawing.Size(149, 26);
            this.tbSongLength.TabIndex = 4;
            // 
            // cbSongArtist
            // 
            this.cbSongArtist.FormattingEnabled = true;
            this.cbSongArtist.Location = new System.Drawing.Point(161, 72);
            this.cbSongArtist.Name = "cbSongArtist";
            this.cbSongArtist.Size = new System.Drawing.Size(267, 28);
            this.cbSongArtist.TabIndex = 7;
            // 
            // Overview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(926, 414);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbTitle);
            this.Controls.Add(this.lbList);
            this.Name = "Overview";
            this.Text = "Overview";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbList;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btArtistAdd;
        private System.Windows.Forms.Button btArtistDelete;
        private System.Windows.Forms.Button btArtistEdit;
        private System.Windows.Forms.TextBox tbArtistName;
        private System.Windows.Forms.Button btSongAdd;
        private System.Windows.Forms.Button btSongDelete;
        private System.Windows.Forms.Button btSongEdit;
        private System.Windows.Forms.TextBox tbSongName;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbSongLength;
        private System.Windows.Forms.ComboBox cbSongArtist;
    }
}

