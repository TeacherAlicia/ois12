﻿namespace HelloLists
{
    partial class HelloLists
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbPlant = new System.Windows.Forms.ListBox();
            this.lbHuman = new System.Windows.Forms.ListBox();
            this.lbTitle = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.tbSentence = new System.Windows.Forms.TextBox();
            this.btAdd = new System.Windows.Forms.Button();
            this.btDelete = new System.Windows.Forms.Button();
            this.btEdit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbPlant
            // 
            this.lbPlant.FormattingEnabled = true;
            this.lbPlant.ItemHeight = 20;
            this.lbPlant.Location = new System.Drawing.Point(12, 98);
            this.lbPlant.Name = "lbPlant";
            this.lbPlant.Size = new System.Drawing.Size(299, 224);
            this.lbPlant.TabIndex = 0;
            // 
            // lbHuman
            // 
            this.lbHuman.FormattingEnabled = true;
            this.lbHuman.ItemHeight = 20;
            this.lbHuman.Location = new System.Drawing.Point(328, 98);
            this.lbHuman.Name = "lbHuman";
            this.lbHuman.Size = new System.Drawing.Size(299, 224);
            this.lbHuman.TabIndex = 1;
            this.lbHuman.SelectedIndexChanged += new System.EventHandler(this.lbHuman_SelectedIndexChanged);
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F);
            this.lbTitle.Location = new System.Drawing.Point(2, 9);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(244, 55);
            this.lbTitle.TabIndex = 2;
            this.lbTitle.Text = "Hello Lists";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(646, 98);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(370, 26);
            this.tbName.TabIndex = 3;
            // 
            // tbSentence
            // 
            this.tbSentence.Location = new System.Drawing.Point(646, 142);
            this.tbSentence.Name = "tbSentence";
            this.tbSentence.Size = new System.Drawing.Size(370, 26);
            this.tbSentence.TabIndex = 4;
            // 
            // btAdd
            // 
            this.btAdd.Location = new System.Drawing.Point(941, 187);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(75, 35);
            this.btAdd.TabIndex = 5;
            this.btAdd.Text = "Add";
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // btDelete
            // 
            this.btDelete.Location = new System.Drawing.Point(837, 187);
            this.btDelete.Name = "btDelete";
            this.btDelete.Size = new System.Drawing.Size(98, 35);
            this.btDelete.TabIndex = 6;
            this.btDelete.Text = "Delete";
            this.btDelete.UseVisualStyleBackColor = true;
            // 
            // btEdit
            // 
            this.btEdit.Location = new System.Drawing.Point(733, 187);
            this.btEdit.Name = "btEdit";
            this.btEdit.Size = new System.Drawing.Size(98, 35);
            this.btEdit.TabIndex = 7;
            this.btEdit.Text = "Edit";
            this.btEdit.UseVisualStyleBackColor = true;
            // 
            // HelloLists
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 337);
            this.Controls.Add(this.btEdit);
            this.Controls.Add(this.btDelete);
            this.Controls.Add(this.btAdd);
            this.Controls.Add(this.tbSentence);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lbTitle);
            this.Controls.Add(this.lbHuman);
            this.Controls.Add(this.lbPlant);
            this.Name = "HelloLists";
            this.Text = "Hello Lists";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lbPlant;
        private System.Windows.Forms.ListBox lbHuman;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.TextBox tbSentence;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Button btDelete;
        private System.Windows.Forms.Button btEdit;
    }
}

