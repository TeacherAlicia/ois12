﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloLists
{
    class Plant
    {
        private string name = "Iris";
        private string colour = "Paars";

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string Colour
        {
            get
            {
                return this.colour;
            }
            set
            {
                this.colour = value;
            }
        }

        public Plant()
        {

        }
    }
}
