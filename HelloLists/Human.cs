﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloLists
{
    class Human
    {
        //variables/fields
        private string sentence = "Hello world";
        private string name = "Alicia";

        //properties
        public string Sentence
        {
            get
            {
                return this.sentence;
            }
            set
            {
                this.sentence = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        //constructor
        public Human()
        {
        }

        //ToString Method
        public override string ToString()
        {
            return this.Name + ": " + this.Sentence;
        }
    }
}
