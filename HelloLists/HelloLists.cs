﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelloLists
{
    public partial class HelloLists : Form
    {
        //dit zijn lists (verzameling) waar plant of human in mogen worden gezet.
        private List<Plant> plants = new List<Plant>();
        private List<Human> humans = new List<Human>();

        public HelloLists()
        {
            InitializeComponent();

            this.plants.Add(new Plant() { Name = "Paardenbloem", Colour = "Geel" });
            this.plants.Add(new Plant() { Name = "Cactus", Colour = "Groen" });
            this.plants.Add(new Plant() { Name = "Lavendel", Colour = "Paars" });
            this.plants.Add(new Plant() { Name = "Roos", Colour = "Rood" });

            this.humans.Add(new Human() { Name = "Alicia", Sentence = "Spoken bestaan niet" });
            this.humans.Add(new Human() { Name = "Marielle", Sentence = "Ik schop tegen een bal" });
            this.humans.Add(new Human() { Name = "Kim", Sentence = "Ik heet Kim Kong" });
            this.humans.Add(new Human() { Name = "Thomas", Sentence = "Deze website is dope" });
            this.humans.Add(new Human() { Name = "Paul", Sentence = "Hardrock is voor watjes" });

            this.SetLists();
        }

        public void SetLists()
        {
            lbPlant.Items.Clear();
            lbHuman.Items.Clear();
            
            //Zonder ToString Method
            foreach (Plant plant in this.plants)
            {
                lbPlant.Items.Add(plant);
            }

            //Met ToString Method
            foreach (Human human in this.humans)
            {
                lbHuman.Items.Add(human);
            }
        }

        private void lbHuman_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Van de geselecteerde item in de listbox human maar ik weer een object (Human). Dit noem je "casting".
            //Hierdoor kan je gemakkelijk weer gegevens uit een object halen (tbName.Text = human.Name);
            Human human = (Human)lbHuman.SelectedItem;

            tbName.Text = human.Name;
            tbSentence.Text = human.Sentence;
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            this.humans.Add(new Human() { Name = tbName.Text, Sentence = tbSentence.Text });

            this.SetLists();
        }
    }
}
