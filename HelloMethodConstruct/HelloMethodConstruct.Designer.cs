﻿namespace HelloMethodConstruct
{
    partial class HelloMethodConstruct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbAdd1 = new System.Windows.Forms.Label();
            this.lbAdd2 = new System.Windows.Forms.Label();
            this.lbAdd3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbAdd1
            // 
            this.lbAdd1.AutoSize = true;
            this.lbAdd1.Location = new System.Drawing.Point(12, 9);
            this.lbAdd1.Name = "lbAdd1";
            this.lbAdd1.Size = new System.Drawing.Size(51, 20);
            this.lbAdd1.TabIndex = 0;
            this.lbAdd1.Text = "label1";
            // 
            // lbAdd2
            // 
            this.lbAdd2.AutoSize = true;
            this.lbAdd2.Location = new System.Drawing.Point(12, 53);
            this.lbAdd2.Name = "lbAdd2";
            this.lbAdd2.Size = new System.Drawing.Size(51, 20);
            this.lbAdd2.TabIndex = 1;
            this.lbAdd2.Text = "label2";
            // 
            // lbAdd3
            // 
            this.lbAdd3.AutoSize = true;
            this.lbAdd3.Location = new System.Drawing.Point(12, 96);
            this.lbAdd3.Name = "lbAdd3";
            this.lbAdd3.Size = new System.Drawing.Size(51, 20);
            this.lbAdd3.TabIndex = 2;
            this.lbAdd3.Text = "label3";
            // 
            // HelloMethodConstruct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(798, 613);
            this.Controls.Add(this.lbAdd3);
            this.Controls.Add(this.lbAdd2);
            this.Controls.Add(this.lbAdd1);
            this.Name = "HelloMethodConstruct";
            this.Text = "Hello Method Construct";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbAdd1;
        private System.Windows.Forms.Label lbAdd2;
        private System.Windows.Forms.Label lbAdd3;
    }
}

