﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloMethodConstruct
{
    class Human
    {
        //variables/fields
        private string sentence;

        //properties
        public string Sentence
        {
            get
            {
                return this.sentence;
            }
            set
            {
                if (value == "henk")
                {
                    this.sentence = "ik zeg niet henk";
                }
                else
                {
                    this.sentence = value;
                }

            }
        }

        //constructor
        public Human(string p_sentence = "Hello World")
        {
            this.Sentence = p_sentence;
        }

        public Human(string p_sentence = "Hello World", int p_age = 0)
        {
            this.Sentence = p_sentence + "- leeftijd: " + Convert.ToString(p_age);
        }

        //methoden
        public int CountCharacters()
        {
            return this.Sentence.Length;
        }
    }
}
