﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HelloMethodConstruct
{
    public partial class HelloMethodConstruct : Form
    {
        //Method overloading: afhankelijk van de parameters die je meegeeft, voert het programma een method uit

        public HelloMethodConstruct()
        {
            InitializeComponent();

            lbAdd1.Text = Convert.ToString(this.Add(1, 2));
            lbAdd2.Text = Convert.ToString(this.Add(1, 2, 4));
            lbAdd3.Text = Convert.ToString(this.Add(5.4, 2.6, 5.8));

            //Method overloading kan je ook bij een constructor doen (zie Human class)
            Human alicia = new Human("programmeren is leuk!");
            Human henkie = new Human("ik heet henkie", 93);
        }

        private int Add(int number1, int number2)
        {
            return number1 + number2;
        }

        private int Add(int number1, int number2, int number3)
        {
            return number1 + number2 + number3;
        }

        private double Add(double number1, double number2, double number3)
        {
            return number1 + number2 + number3;
        }
    }
}
